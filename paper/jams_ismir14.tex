% -----------------------------------------------
% Template for ISMIR 2014
% (based on earlier ISMIR templates)
% -----------------------------------------------

\documentclass{article}
\usepackage{ismir2014,amsmath,cite}
\usepackage{graphicx}

% Title.
% ------
\title{These are my {JAMS}: {JSON} {A}nnotated {M}usic {S}pecification for
Reproducible {MIR} Research}

% Single address
% To use with only one author or several with the same address
% ---------------
\oneauthor
{John, Paul, George, Ringo}
{$^1$Music and Audio Research Lab, Steinhardt School of Many Things \\
$^2$Center for Urban Science, Progress, and All That Jazz \\
New York University, New York, USA \\ 
{\tt \{john,paul,george,ringo\}@nyu.edu}}

% Two addresses
% --------------
%\twoauthors
%  {First author} {School \\ Department}
%  {Second author} {Company \\ Address}

% Three addresses
% --------------
% \threeauthors
%   {First author} {Affiliation1 \\ {\tt author1@ismir.edu}}
%   {Second author} {\bf Retain these fake authors in\\\bf submission to preserve the formatting}
%   {Third author} {Affiliation3 \\ {\tt author3@ismir.edu}}

% Four addresses
% --------------
%\fourauthors
%  {First author} {Affiliation1 \\ {\tt author1@ismir.edu}}
%  {Second author}{Affiliation2 \\ {\tt author2@ismir.edu}}
%  {Third author} {Affiliation3 \\ {\tt author3@ismir.edu}}
%  {Fourth author} {Affiliation4 \\ {\tt author4@ismir.edu}}

\begin{document}
%
\maketitle
%
\begin{abstract}
Lorem ipsum dolor sit amet, et alienum fabellas expetenda usu, et vel choro dolor. No vel consulatu vulputate vituperata. Inermis officiis eleifend ad cum, per quem unum quando te, et libris quodsi erroribus mel. Quot tibique concludaturque et pro, latine dissentiet mei an, quidam commune scaevola qui no. Ridens praesent usu ea, nihil integre sed id.
 
\end{abstract}
%
\section{Introduction}
\label{sec:introduction}

Driven by ever-growing, web-scale collections of digital audio, embodied by companies like Spotify\footnote{http://www.spotify.com}, Soundcloud\footnote{http://www.soundcloud.com}, and YouTube\footnote{http://www.youtube.com}, content-based music informatics research (MIR) is an active topic receiving significant interest by the research community.
Stated simply, this area of study attempts to extract or infer information from and about digital music signals.
By its very nature, content-based research relies heavily on annotated music signals for the development and evaluation of computational systems.

% Traditionally these are lab files, and they get used for everything.
Traditionally annotations have taken the form of LAB files, which are text-based, human readable representations of observed musical concepts for some task, such as chord recognition, continuous f0 prediction, beat estimation, or section labeling; an example chord annotation is given in Figure \ref{fig:chord_lab}.
% There are good reasons why they've been used.
For over a decade, LAB files have addressed the need for a cross-platform, language independent data format that could be produced or consumed with minimal overhead.
% They are not without problems.
Despite this apparent simplicity, however, this format is not without its deficiencies and practical limitations.
% Problems with lab-files
% -----------------------
Firstly, the schema of a LAB file is unique to a given task, and therefore different parsers are necessary to translate different kinds of annotations from ASCII characters on disk to programmatic data structures in memory.
This can be particularly troublesome when using LAB files because the schema of the data contained therein is not explicitly encoded in the format, but rather \emph{implied} by the task and must be known a priori.
Additionally, there is no official standard or specification, making it challenging to associate metadata or any other side-information in the body of a given annotation.

To solve these problems with minimal overhead, while providing a solution that will scale well into the next decade of MIR, we propose a JSON Annotated Music Specification (JAMS), a modern approach based on three design tenets: simplicity, structure, and sustainability. 

\section{Motivation}

Mention previous work. Reproducible research. Sustainability. Best practices.
Geoffroy, SoundSoftware, other things that are of increasing importance to the research community.
Code-sharing, version control, etc.

\subsection{Design Principles}
\label{sec:motivation}

To address the issues outlined previously, it is necessary to realize that data persistence consists of two distinct operations: deserializing data from file into a programmatic data structure, and making use of that in-memory data structure.

\subsubsection{Structure}
The key change introduced by JAMS is that of hierarchical structure in the data, and it is this modification that enables great power from a tiny increase in complexity.
Named entities.

\subsubsection{Simplicity}
These should be human-readable, obvious by inspection, and decoupled from both programming language and operating system.
It should leverage existing technologies whenever possible to remove the need for file-specific parsers, and make it trivial to go from ASCII to a useful data structure in someone's language of choice.


\subsubsection{Sustainability}

\subsection{Design Decisions}
JSON for it's speed and diverse support.

\section{The JAMS Specification}
\label{sec:spec}

The JAMS specification is a hierarchical structure. A diagram of this structure
is provided in Figure \ref{fig:jams_structure}. At the top level there are two
containers: \textit{data} and \textit{metadata}. As their names suggest, the
\textit{data} container holds the annotation of a musical attribute (e.g.~chord
data, onsets, melody $f_0$ values, etc.) whilst the \textit{metadata} container holds
information about the audio file (e.g.~title, artist, Music Brainz ID, md5
hash, etc.) and the annotation process (annotator, tools used, version,
etc.). In Sections \ref{sec:spec:data} and \ref{sec:spec:metadata} we describe
each of the two containers in detail. Finally in Section \ref{sec:spec:schema}
we describe how the JAMS specification is formally defined using JSON schemata.

\subsection{Data}
\label{sec:spec:data}

The \textit{data} container holds the musical information that has been
annotated for the audio file, such as its chord sequence, beat locations, etc.
When considering the different types of musical attributes annotated for MIR
research, one can (for the most part) divide them into four categories:
\begin{enumerate}
  \item Attributes that consist of a single \textit{observation} for the entire
  audio file, e.g.~a musical genre.
  \item Attributes that consist of \textit{events} that occur at specific times,
  e.g.~beats or onsets.
  \item Attributes that consist of observations that span a certain time
  \textit{range} of the audio, such as chords or sections.
  \item Attributes that consist of observations in the form of a dense
  \textit{time series}, such as $f_0$ values sampled at a fixed time interval
  (e.g.~10 ms) in the case of pitch tracking or melody extraction.
\end{enumerate}

Consequently, the JAMS specification defines 4 atomic types, out of which an
annotation can be constructed: \textit{observation}, \textit{event},
\textit{range} and \textit{time series}. The \textit{data} container is simply
an array of elements that are all of a specific atomic type. For instace, the
\textit{data} container in a JAMS onset annotation will be an array of \textit{event}
elements, and for a chord annotation it will be an array of \textit{range}
elements. For melody extraction it will be an array with a single \textit{time series}
element. For genre it would be an array with a single
\textit{observation} element, and for auto-tagging (e.g.~instrumentation) it
would be an array with multiple \textit{observation} elements. The structure of
the four atomic types is described below.

\subsubsection{Observation}
An \textit{observation} is the simplest of the 4 atomic types. It is a container
that holds one main field, \textit{value}, and two optional
fields, \textit{confidence} and \textit{context}. The \textit{value} field can
have any type, for example a string (e.g.~for storing the name of a genre), a
numeric value (e.g.~tonic pitch) or a boolean. The optional \textit{confidence}
field stores a numeric confidence estimate for the value stored in
\textit{value}. Finally, the optional \textit{context} field stores a string
value and is provided for flexibility in case of observations that require two
different strings (one in \textit{value} and another in \textit{context}). An
example of the \textit{data} container for a JAMS genre annotation is provided
in Figure \ref{fig:jams_genre}.

\subsubsection{Event}
The \textit{event} atomic type is useful for representing musical attributes
that occur at specific moments in time, such as beats or onsets.  It is a
container that holds two elements: \textit{time} and \textit{label}, both of
which are of type \textit{observation}. That is, \textit{time} is an observation
with a (numeric) value and possibly a confidence estimate, and \textit{label} is
an observation with a string value and possibly a confidence of its own. An example of the
\textit{data} container for a JAMS onset annotation is provided in Figure
\ref{fig:jams_onset}.

\subsubsection{Range}
The \textit{range} atomic type is useful for representing musical attributes
that span a time range of the audio file, such as chords or song sections
(i.e.~intro, verse, chrous, etc.). It is a container that holds three elements:
\textit{start}, \textit{end}, and \textit{label}. Like in the \textit{event}
atomic type, each of these elements is of type \textit{observation}. An
example of the \textit{data} container for a JAMS chord annotation is provided
in Figure \ref{fig:jams_chord}.

\subsubsection{Time Series}
The \textit{time series} atomic type is useful for representing musical
attributes that have a continuous nature, such as pitch ($f_0$) over time. It is
a container that holds four elements: \textit{label}, \textit{value},
\textit{time}, and \textit{confidence}. The \textit{label} element is
of type \textit{observation}, holding a string value (e.g.~the name of the instrument
whose pitch has been annotated) and possibly a confidence value (in the case of
an instrument label there would be no need for a confidence value). Each of the
remaining three elements is an array of numerical values: \textit{time} holds
the timestamps of the time series, \textit{values} holds the corresponding value
(e.g.~$f_0$) for each timestamp, and \textit{confidnence} the corresponding
confidence for each value. An example of the \textit{data} container for a JAMS
melody annotation is provided in Figure \ref{fig:jams_melody}.

\subsection{Metadata}
\label{sec:spec:metadata}

\subsubsection{File metadata}
\label{sec:spec:metadata:file_metadata}

\subsubsection{Annotation metadata}  
\label{sec:spec:metadata:annotation_metadata}

\subsection{The JAMS JSON Schema}
\label{sec:spec:schema}

\section{Using JAMS}
\label{sec:using_jams}

\subsection{Examples}

\subsection{Tools}

\subsection{Datasets}

\section{Future Perspectives and Conclusion}
\label{sec:conclusion}

% PLEASE LEAVE THIS CODE HERE FOR REFERENCE
% \begin{table}
%  \begin{center}
%  \begin{tabular}{|l|l|}
%   \hline
%   String value & Numeric value \\
%   \hline
%   Hello ISMIR  & 2014 \\
%   \hline
%  \end{tabular}
% \end{center}
%  \caption{Table captions should be placed below the table.}
%  \label{tab:example}
% \end{table}
% 
% \begin{figure}
%  \centerline{\framebox{
%  \includegraphics[width=\columnwidth]{figure.png}}}
%  \caption{Figure captions should be placed below the figure.}
%  \label{fig:example}
% \end{figure}
% 
% \section{Equations}
% 
% Equations should be placed on separated lines and numbered.
% The number should be on the right side, in parentheses.
% 
% \begin{equation}
% E=mc^{2}
% \end{equation}

\bibliography{jams_bibliography}

\end{document}
